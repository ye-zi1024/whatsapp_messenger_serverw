package com.finn.domain.message.entity;

import java.util.Date;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import lombok.Data;

/**
 * Order.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@Data
@Table("message")
public class Message {

	@Id(keyType = KeyType.Auto)
	private Long id;

	private String receiverId;

	private String textMessage;

	private String type;

	private Date timeSent;

	private String messageId;

	private Boolean isSeen;

	public void setTimeSent(Date timeSent) {
		if (timeSent != null) {
			this.timeSent = (Date) timeSent.clone();
		}
		else {
			this.timeSent = null;
		}
	}

	public Date getTimeSent() {
		if (this.timeSent != null) {
			return (Date) this.timeSent.clone();
		}
		return null;
	}

}
