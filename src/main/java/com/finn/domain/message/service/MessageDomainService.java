package com.finn.domain.message.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.finn.domain.message.entity.Message;
import com.finn.domain.message.repository.MessageMapper;
import com.finn.infrastructure.common.event.EventPublisher;

import lombok.extern.slf4j.Slf4j;

/**
 * OrderDomainService.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@Service
@Slf4j
public class MessageDomainService {

	@Autowired
	EventPublisher eventPublisher;

	@Autowired
	MessageMapper messageMapper;

	public Message getMessageById(String messageId) {
		return this.messageMapper.getMessageById(messageId);
	}

	@Transactional
	public Message createMessage(Message message) {
		Integer count = this.messageMapper.createMessage(message);
		System.out.println("count = " + count);
		// // 发送领域事件
		// MessageEvent event = MessageEvent.create(MessageEventType.CREATE_EVENT,
		// message);
		// this.eventPublisher.publish(event);
		return message;
	}

}
