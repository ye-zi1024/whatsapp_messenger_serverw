package com.finn.domain.message.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.finn.domain.message.entity.Message;
import com.mybatisflex.core.BaseMapper;
//import org.apache.ibatis.annotations.*;

/**
 * OrderRepositoryInterface.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@Mapper
public interface MessageMapper extends BaseMapper<Message> {

	/**
	 * getMessageById.
	 * @param messageId 消息id
	 * @return 消息
	 */
	@Select("select * from `message` where message_id = #{messageId}")
	Message getMessageById(@Param("messageId") String messageId);

	/**
	 * createMessage.
	 * @param message 订单对象
	 * @return 创建成功条数
	 */
	@Insert("INSERT INTO `message` " + "(id,sender_id, receiver_id, text_message, type, time_sent,message_id,is_seen) "
			+ "VALUES ( #{message.id}, #{message.senderId}, #{message.receiverId},#{message.textMessage}, "
			+ "#{message.type}, #{message.timeSent}, #{message.messageId}, #{message.isSeen})")
	Integer createMessage(@Param("message") Message message);

}
