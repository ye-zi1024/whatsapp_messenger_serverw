package com.finn.domain.message.event;

import java.util.Date;

import com.alibaba.fastjson2.JSON;
import com.finn.domain.message.entity.Message;
import com.finn.infrastructure.common.event.DomainEvent;
import com.finn.infrastructure.util.IdGenerator;

/**
 * OrderEvent.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
public class MessageEvent extends DomainEvent {

	MessageEventType messageEventType;

	public static MessageEvent create(MessageEventType eventType, Message message) {
		MessageEvent event = new MessageEvent();
		event.setId(IdGenerator.nextStrId());
		event.setOrderEventType(eventType);
		event.setTimestamp(new Date());
		event.setData(JSON.toJSONString(message));
		return event;
	}

	public MessageEventType getOrderEventType() {
		return this.messageEventType;
	}

	public void setOrderEventType(MessageEventType messageEventType) {
		this.messageEventType = messageEventType;
	}

}
