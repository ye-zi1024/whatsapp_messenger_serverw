package com.finn.domain.message.event;

/**
 * OrderEventType.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
public enum MessageEventType {

	/**
	 * CREATE_EVENT.
	 */
	CREATE_EVENT,

	/**
	 * SECKILL_EVENT.
	 */
	SECKILL_EVENT

}
