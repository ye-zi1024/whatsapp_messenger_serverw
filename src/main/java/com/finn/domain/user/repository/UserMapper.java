package com.finn.domain.user.repository;

import org.apache.ibatis.annotations.Mapper;

import com.finn.domain.user.entity.User;
import com.mybatisflex.core.BaseMapper;

/**
 * accountMapper.
 *
 * @author Li Zemin
 * @since 2023/6/27 17:36
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
