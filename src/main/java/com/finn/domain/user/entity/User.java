package com.finn.domain.user.entity;

import java.io.Serializable;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;

import lombok.Data;

/**
 * user.
 *
 * @author Li Zemin
 * @since 2023/6/27 17:36
 */
@Data
@Table("user")
public class User implements Serializable {

	@Id(keyType = KeyType.Auto)
	private Long id;

	private String userName;

	private String uid;

	private String profileImageUrl;

	private Boolean active;

	private Integer lastSeen;

	final String phoneNumber;

	final String groupId;

}
