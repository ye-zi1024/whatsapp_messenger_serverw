package com.finn.interfaces.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * accountDTO.
 *
 * @author Li Zemin
 * @since 2023/6/27 17:35
 */
@Data
public class UserDTO implements Serializable {

	private String userName;

	private String uid;

	private String profileImageUrl;

	private Boolean active;

	private Integer lastSeen;

	private String phoneNumber;

	private String groupId;

}
