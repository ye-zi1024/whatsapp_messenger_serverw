package com.finn.interfaces.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * MessageDTO.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@Data
public class MessageDTO implements Serializable {

}
