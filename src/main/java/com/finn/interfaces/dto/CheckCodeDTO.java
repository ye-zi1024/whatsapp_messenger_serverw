package com.finn.interfaces.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * 验证码.
 *
 * @author Li ZeMin
 * @since create in 2023-09-17 16:32
 */
@Data
public class CheckCodeDTO implements Serializable {

	private String phoneNumber;

	private String code;

}
