package com.finn.interfaces.assembler;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import com.finn.domain.message.entity.Message;
import com.finn.interfaces.dto.MessageDTO;

/**
 * OrderAssembler.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MessageAssembler {

	/**
	 * 对象转换.
	 * @param message 订单对象
	 * @return 订单传输对象
	 */
	MessageDTO toDTO(Message message);

}
