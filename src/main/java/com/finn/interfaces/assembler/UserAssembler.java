package com.finn.interfaces.assembler;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import com.finn.domain.user.entity.User;
import com.finn.interfaces.dto.UserDTO;

/**
 * accountAssembler.
 *
 * @author Li Zemin
 * @since 2023/6/27 17:34
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserAssembler {

	/**
	 * 对象转换.
	 * @param user 用户对象
	 * @return 用户对象
	 */
	UserDTO toDTO(User user);

}
