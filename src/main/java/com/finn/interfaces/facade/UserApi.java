package com.finn.interfaces.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.finn.application.service.UserApplicationService;
import com.finn.domain.user.entity.User;
import com.finn.infrastructure.common.api.Response;
import com.finn.interfaces.assembler.UserAssembler;
import com.finn.interfaces.dto.CheckCodeDTO;

import lombok.extern.slf4j.Slf4j;

/**
 * AccountApi.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@RestController
@RequestMapping("/api/v1/user")
@Slf4j
public class UserApi {

	@Autowired
	UserApplicationService userApplicationService;

	@Autowired(required = false)
	UserAssembler userAssembler;

	@GetMapping("/detail/{userId}")
	public Response findById(@PathVariable String userId) {
		User user = this.userApplicationService.getUserByUid(userId);
		return Response.ok(this.userAssembler.toDTO(user));
	}

	@PostMapping("/create")
	public Response create(@RequestBody User user) {
		User user1 = this.userApplicationService.createUser(user);
		return Response.ok(user1);
	}

	/**
	 * 根据手机号生成登录验证码.
	 * @param user 手机号
	 * @return response
	 */
	@PostMapping("/code")
	public Response code(@RequestBody User user) {
		String code = this.userApplicationService.generateCode(user.getPhoneNumber());
		return Response.ok(code);
	}

	@PostMapping("/checkCode")
	public Response checkCode(@RequestBody CheckCodeDTO codeDTO) {
		Boolean res = this.userApplicationService.checkCode(codeDTO);
		return Response.ok(res);
	}

}
