package com.finn.interfaces.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.finn.application.service.MessageApplicationService;
import com.finn.domain.message.entity.Message;
import com.finn.infrastructure.common.api.Response;
import com.finn.interfaces.assembler.MessageAssembler;

import lombok.extern.slf4j.Slf4j;

/**
 * MessageApi.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@RestController
@RequestMapping("/api/v1/message")
@Slf4j
public class MessageApi {

	@Autowired
	MessageApplicationService messageApplicationService;

	@Autowired(required = false)
	MessageAssembler messageAssembler;

	@RequestMapping("/detail/{messageId}")
	@ResponseBody
	public Response findById(@PathVariable String messageId) {
		Message message = this.messageApplicationService.getMessageById(messageId);
		return Response.ok(this.messageAssembler.toDTO(message));
	}

	@RequestMapping("/create")
	@ResponseBody
	public Response create(@RequestBody Message message) {
		Message message1 = this.messageApplicationService.createMessage(message);
		return Response.ok(message1);
	}

}
