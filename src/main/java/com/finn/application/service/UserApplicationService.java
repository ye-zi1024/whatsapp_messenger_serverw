package com.finn.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finn.domain.user.entity.User;
import com.finn.domain.user.service.UserDomainService;
import com.finn.interfaces.dto.CheckCodeDTO;

/**
 * accountApplicationService.
 *
 * @author Li Zemin
 * @since 2023/6/27 17:36
 */
@Service
public class UserApplicationService {

	@Autowired
	UserDomainService userDomainService;

	public User getUserByUid(String uid) {
		return this.userDomainService.getUserByUid(uid);
	}

	public User createUser(User user) {
		return this.userDomainService.createUser(user);
	}

	public String generateCode(String phoneNumber) {
		return this.userDomainService.generateCode(phoneNumber);
	}

	public Boolean checkCode(CheckCodeDTO codeDTO) {
		return this.userDomainService.checkCode(codeDTO);
	}

}
