package com.finn.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finn.domain.message.entity.Message;
import com.finn.domain.message.service.MessageDomainService;

/**
 * OrderApplicationService.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:42 AM
 */
@Service
public class MessageApplicationService {

	@Autowired
	MessageDomainService messageDomainService;

	public Message getMessageById(String messageId) {
		return this.messageDomainService.getMessageById(messageId);
	}

	public Message createMessage(Message message) {
		return this.messageDomainService.createMessage(message);
	}

}
