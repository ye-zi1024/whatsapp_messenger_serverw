package com.finn.infrastructure.mybatis.flex.audit;

import java.util.List;

import com.mybatisflex.core.audit.AuditMessage;
import com.mybatisflex.core.audit.MessageReporter;

/**
 * .
 *
 * @author Li ZeMin
 *
 */
public class MyMessageReporter implements MessageReporter {

	@Override
	public void sendMessages(List<AuditMessage> messages) {
		// 在这里把 messages 审计日志发送到指定位置
		// 比如
		// 1、通过 http 协议发送到指定服务器
		// 2、通过日志工具发送到日志平台
		// 3、通过 Kafka 等 MQ 发送到指定平台
	}

}
