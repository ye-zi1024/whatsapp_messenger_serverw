package com.finn.infrastructure.mybatis.flex.audit;

import java.util.List;

import com.mybatisflex.core.audit.AuditMessage;
import com.mybatisflex.core.audit.MessageReporter;

/**
 * .
 *
 * @author Li ZeMin
 *
 */
public class ConsoleMessageReporter implements MessageReporter {

	@Override
	public void sendMessages(List<AuditMessage> messages) {
		for (AuditMessage message : messages) {
			System.out.println(">>>>>>Sql Audit: " + message.toString());
		}
	}

}
