package com.finn.infrastructure.mybatis.flex;

import org.springframework.context.annotation.Configuration;

import com.finn.infrastructure.mybatis.flex.audit.ConsoleMessageReporter;
import com.finn.infrastructure.mybatis.flex.audit.MyMessageFactory;
import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import com.mybatisflex.core.audit.ConsoleMessageCollector;
import com.mybatisflex.core.audit.MessageCollector;
import com.mybatisflex.core.audit.MessageFactory;
import com.mybatisflex.spring.boot.MyBatisFlexCustomizer;

/**
 * .
 *
 * @author Li ZeMin
 *
 */
@Configuration
public class MyBatisFlexConfiguration implements MyBatisFlexCustomizer {

	@Override
	public void customize(FlexGlobalConfig globalConfig) {
		// 我们可以在这里进行一些列的初始化配置

		MessageFactory creator = new MyMessageFactory();
		AuditManager.setMessageFactory(creator);

		ConsoleMessageReporter reporter = new ConsoleMessageReporter();
		AuditManager.setMessageReporter(reporter);

		MessageCollector collector = new ConsoleMessageCollector();
		AuditManager.setMessageCollector(collector);

		AuditManager.setAuditEnable(true);

	}

}
