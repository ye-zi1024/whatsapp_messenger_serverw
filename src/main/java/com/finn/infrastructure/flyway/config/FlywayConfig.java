// package com.finn.infrastructure.dbconfig;
//
// import javax.annotation.PostConstruct;
// import javax.sql.DataSource;
//
// import org.flywaydb.core.Flyway;
// import org.flywaydb.core.api.FlywayException;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.context.annotation.Configuration;
//
// @Configuration
// public class FlywayConfig {
//
// @Autowired
// private DataSource dataSource;
//
// private Logger logger = LoggerFactory.getLogger(this.getClass());
//
// @PostConstruct
// public void migrate() {
// Flyway flyway = Flyway.configure()
// .dataSource(dataSource)
// .locations("db/migration")
// .encoding("UTF-8")
// .outOfOrder(true)
// .load();
// try {
// flyway.migrate();
// }
// catch (FlywayException e) {
// flyway.repair();
// logger.error("Flyway配置加载出错", e);
// }
// }
//
// }
