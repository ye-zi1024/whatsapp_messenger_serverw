package com.finn.infrastructure.common.event;

import java.util.Date;

/**
 * DomainEvent.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
public class DomainEvent {

	String id;

	Date timestamp;

	String source;

	String data;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public void setTimestamp(Date timestamp) {
		if (timestamp != null) {
			this.timestamp = (Date) timestamp.clone();
		}
		else {
			this.timestamp = null;
		}
	}

	public Date getTimestamp() {
		if (this.timestamp != null) {
			return (Date) this.timestamp.clone();
		}
		return null;
	}

}
