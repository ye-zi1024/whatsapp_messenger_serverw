package com.finn.infrastructure.common.event;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson2.JSONObject;
import com.finn.domain.message.event.MessageEvent;

/**
 * EventPublisher.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
@Service
public class EventPublisher {

	public void publish(MessageEvent event) {
		// send to MQ
		// mq.send(event);
		System.out.println("event = " + JSONObject.toJSONString(event));
	}

}
