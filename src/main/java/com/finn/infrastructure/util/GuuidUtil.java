package com.finn.infrastructure.util;

/**
 * GuuidUtil.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
public final class GuuidUtil {

	private static long machineId = 0;

	private static long datacenterId = 0;

	private GuuidUtil() {
	}

	/**
	 * 单例模式创建学法算法对象.
	 */

	private enum SnowFlakeSingleton {

		/**
		 * Singleton.
		 */
		Singleton;

		private SnowFlake snowFlake;

		SnowFlakeSingleton() {
			this.snowFlake = new SnowFlake(datacenterId, machineId);
		}

		public SnowFlake getInstance() {
			return this.snowFlake;
		}

	}

	public static long getUuid() {
		return SnowFlakeSingleton.Singleton.getInstance().nextId();
	}

}
