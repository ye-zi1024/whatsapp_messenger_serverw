package com.finn.infrastructure.util;

import java.security.SecureRandom;

/**
 * 验证码工具类.
 *
 * @author Li ZeMin
 * @since create in 2023-09-17 16:32
 */
public final class VerificationCodeUtil {

	private VerificationCodeUtil() {
	}

	/**
	 * 随机生成6位验证码.
	 * @return 验证码
	 */
	public static String generateVerificationCode() {
		int codeLength = 6;
		SecureRandom random = new SecureRandom();
		StringBuilder code = new StringBuilder();

		for (int i = 0; i < codeLength; i++) {
			int digit = random.nextInt(10);
			code.append(digit);
		}

		return code.toString();
	}

}
