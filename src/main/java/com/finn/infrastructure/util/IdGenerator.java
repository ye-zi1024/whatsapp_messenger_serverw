package com.finn.infrastructure.util;

import java.util.UUID;

/**
 * IdGenerator.
 *
 * @author Li ZeMin
 * @since 2020/7/26 1:50 AM
 */
public final class IdGenerator {

	private IdGenerator() {
	}

	public static String nextStrId() {
		return UUID.randomUUID().toString();
	}

	public static Long nextLongId() {
		return GuuidUtil.getUuid();
	}

}
