-- 消息表
CREATE TABLE IF NOT EXISTS message
(
    id                   bigint(20) not null comment '主键Id',
    sender_id            varchar(64) comment '发送者用户Id',
    receiver_id          varchar(64) comment '接收者用户Id',
    text_message         text comment '消息内容',
    type                 varchar(12) comment '消息类型(文本、图片、音频、视频、gif)',
    time_sent            datetime comment '消息发送时间',
    message_id           varchar(64) comment '消息Id',
    is_seen              int(1) comment '是否已读',
    primary key (id)
    );

alter table message comment '消息表';

-- 用户表
CREATE TABLE IF NOT EXISTS user
(
    id                   bigint(20) not null comment '主键Id',
    username             varchar(64) comment '用户名称',
    uid                  varchar(64) comment '业务用户Id',
    profile_image_url    varchar(128) comment '用户头像',
    active               int(1) comment '用户状态',
    last_seen            int(11) comment '最后登录时间',
    phone_number         varchar(11) comment '手机号码',
    groupId              bigint(20) comment '组织Id',
    primary key (id)
    );

alter table user comment '用户表';