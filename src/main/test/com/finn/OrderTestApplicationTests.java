package com.finn;

import java.util.Date;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.finn.domain.user.entity.Account;
import com.finn.domain.message.entity.valueobject.Good;
import com.finn.domain.message.entity.valueobject.User;
import com.finn.infrastructure.common.api.Response;
import com.finn.interfaces.facade.OrderApi;



@SpringBootTest
class OrderTestApplicationTests {

    @Autowired
    private OrderApi orderApi;

    @Test
    @Order(1)
    void contextLoads() {
        com.finn.domain.message.entity.Order order = new com.finn.domain.message.entity.Order();
        order.setOrderId(1L);
        order.setOrderChannel(1);
        order.setPayDate(new Date());
        order.setStatus(1);
        order.setDeliveryAddrId(1L);
        order.setCreateDate(new Date());
        User user = new User();
        user.setUserId(1L);
        user.setUsername("LZM");
        order.setUser(user);
        Good good = new Good();
        good.setGoodsId(1L);
        good.setGoodsName("小熊");
        good.setGoodsCount(1);
        good.setGoodsPrice(100.00);
        order.setGood(good);
        Response response = this.orderApi.create(order);
        System.out.println(response);
    }

    @Test
    @Order(2)
    void addAccount() {
        Response byId = this.orderApi.findById(871185385764421632L);
        System.out.println("byId = " + byId);
    }

}