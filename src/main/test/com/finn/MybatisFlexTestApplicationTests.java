package com.finn;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.finn.domain.user.entity.Account;
import com.finn.domain.user.repository.AccountMapper;
import com.mybatisflex.core.query.QueryCondition;
import com.mybatisflex.core.query.QueryWrapper;

import static com.finn.domain.user.entity.table.AccountTableDef.ACCOUNT;


@SpringBootTest
class MybatisFlexTestApplicationTests {

    @Autowired
    private AccountMapper accountMapper;

    @Test
    @Order(1)
    void contextLoads() {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .select()
                .where(ACCOUNT.AGE.eq(18));
        Account account = this.accountMapper.selectOneByQuery(queryWrapper);
        System.out.println(account);
    }

    @Test
    @Order(2)
    void addAccount() {
        Account account = new Account();
        account.setAge(20);
        account.setUserName("LZM");
        account.setPhone("13115000396");
        int i = this.accountMapper.insertOrUpdate(account);
        System.out.println("i = " + i);
    }

    @Test
    @Order(3)
    void findAccount() {
        QueryWrapper where = new QueryWrapper().where(ACCOUNT.PHONE.eq("13115000396"));
        Account account = this.accountMapper.selectOneByQuery(where);
        System.out.println("account = " + account);
    }

}